#include "TemperatureHumidity.h"
#include <Arduino.h>

#define DEBUG 0

TemperatureHumidity::TemperatureHumidity(DHT &dht, PubSubClient &client, MqttConfig &tempConfig,
                                         MqttConfig &humidityConfig) {
  this->dht = &dht;
  this->client = &client;
  this->temperaturConfig = &tempConfig;
  this->humidityConfig = &humidityConfig;
}

void TemperatureHumidity::begin() {
  this->dht->begin();
  readTemperatureAndHumidity();
}
void TemperatureHumidity::setMeasurementInterval(unsigned long interval) { this->measurementInterval = interval; }

void TemperatureHumidity::loop() {
  unsigned long now = millis();
  if (now - lastMeasurement > measurementInterval) {
    lastMeasurement = now;
    readTemperatureAndHumidity();
    publishTemperature();
    publishHumidity();
  }
}

float TemperatureHumidity::getTemperature() { return this->temperature; }

void TemperatureHumidity::readTemperatureAndHumidity() {
  temperature = this->dht->readTemperature();
  humidity = this->dht->readHumidity();

#if DEBUG >= 1
  Serial.print("Temperature: ");
  Serial.println(temperature);
  Serial.print("Humidity: ");
  Serial.println(humidity);
#endif
}

float TemperatureHumidity::getHumidity() { return this->humidity; }

void TemperatureHumidity::publishTemperature() {
  sprintf(msg, this->temperaturConfig->messageTemplate, temperature);
#if DEBUG > 1
  Serial.println(msg);
#endif
  client->publish(this->temperaturConfig->topicSend, msg, true);
  client->loop();
}

void TemperatureHumidity::publishHumidity() {
  sprintf(msg, this->humidityConfig->messageTemplate, humidity);
#if DEBUG > 1
  Serial.println(msg);
#endif
  client->publish(this->humidityConfig->topicSend, msg, true);
  client->loop();
}
