#include "DHT.h"
#include "MqttConfig.h"
#include "PubSubClient.h"
#include <Arduino.h>

#ifndef TEMPERATUREHUMIDITY_H_
#define TEMPERATUREHUMIDITY_H_

class TemperatureHumidity {
private:
  char msg[256];
  DHT *dht;
  PubSubClient *client;
  MqttConfig *temperaturConfig;
  MqttConfig *humidityConfig;
  float temperature = 0.;
  float humidity = 0.;
  unsigned long lastMeasurement = 0;
  uint32_t measurementInterval = 60000;

public:
  TemperatureHumidity(DHT &dht, PubSubClient &client, MqttConfig &tempConfig, MqttConfig &humidityConfig);
  void begin();
  void loop();
  void setMeasurementInterval(unsigned long interval);
  void readTemperatureAndHumidity();
  float getTemperature();
  float getHumidity();
  void publishTemperature();
  void publishHumidity();
};

#endif /* TEMPERATUREHUMIDITY_H_ */
