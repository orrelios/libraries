#include <Arduino.h>
#include "ThresholdSensor.h"

#define DEBUG 1

/**
 * remember to call begin() in setup and loop() in loop
 */
ThresholdSensor::ThresholdSensor(const uint8_t pinSensor, const uint8_t pMode, boolean highIsActive)
{
    pinMode(pinSensor, pMode);
    this->pinSensor = pinSensor;
    this->highIsActive = highIsActive;
}

void ThresholdSensor::begin()
{
    this->active = digitalRead(this->pinSensor);
    this->previousValue = this->active;
}

void ThresholdSensor::loop()
{
    unsigned long now = millis();

    boolean current = digitalRead(this->pinSensor);
    if (current != previousValue)
    {
        this->lastChange = now;
    }
    if (now - this->lastChange > this->threshold)
    {
        this->active = current;
    }
    previousValue = current;
}

boolean ThresholdSensor::isActive() {
    if(highIsActive) {
        return this->active;
    } else {
        return !this->active;
    }
}

void ThresholdSensor::setThreshold(uint16_t ms)
{
    this->threshold = ms;
}
