
#ifndef MQTTCONFIG_H_
#define MQTTCONFIG_H_

struct MqttConfig
{
	const char *topicSend;
	const char *topicRequest;
	const char *messageTemplate;
};

#endif /* MQTTCONFIG_H_ */
