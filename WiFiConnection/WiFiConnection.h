#include "Arduino.h"
#ifndef WIFICONNECTION_H_
#define WIFICONNECTION_H_
#ifdef ESP8266
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#endif
#ifdef ESP32
#include <WebServer.h>
#include <ESPmDNS.h>
#endif

class WiFiConnection
{
private:
    static String ip2string(IPAddress ip);
    
public:
    static void init_wifi(const char *ssid, const char *password, const char *mdns_hostname);
    static void init_wifi(const char *ssid, const char *password);
    static void init_mdns(const char *host);
};

#endif /* WIFICONNECTION_H_ */
