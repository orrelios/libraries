#include "WiFiConnection.h"

String WiFiConnection::ip2string(IPAddress ip) {
	String ret = String(ip[0]) + "." +  String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);

	return ret;
}

void WiFiConnection::init_wifi(const char *ssid, const char *password) {
    int local_count = 0;
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    Serial.println("");
    Serial.print("Connecting to Wifi");

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(1000);
        if (++local_count == 15) {
            Serial.println("RESTART");
            ESP.restart();
        }
    }

    Serial.println("");
    Serial.printf("Connected to '%s'\r\n\r\n",ssid);

    String ipaddr = ip2string(WiFi.localIP());
    Serial.printf("IP address   : %s\r\n", ipaddr.c_str());
    Serial.printf("MAC address  : %s \r\n", WiFi.macAddress().c_str());
}

void WiFiConnection::init_wifi(const char *ssid, const char *password, const char *mdns_hostname) {
	init_wifi(ssid, password);
	init_mdns(mdns_hostname);
}

void WiFiConnection::init_mdns(const char *host) {
	// Use mdns for host name resolution
	if (!MDNS.begin(host)) {
		Serial.println("Error setting up MDNS responder!");
        return;
	}

	Serial.printf("mDNS started : %s\r\n", host);
}